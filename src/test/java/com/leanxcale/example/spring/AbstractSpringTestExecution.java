package com.leanxcale.example.spring;

import com.leanxcale.example.AbstractTestExecution;
import lombok.extern.slf4j.Slf4j;
import org.springframework.jdbc.core.JdbcTemplate;

@Slf4j
public abstract class AbstractSpringTestExecution extends AbstractTestExecution {

  protected abstract JdbcTemplate getLxJdbcTemplate();

  @Override
  protected void execute(String sql) {
    getLxJdbcTemplate().execute(sql);
  }
}
