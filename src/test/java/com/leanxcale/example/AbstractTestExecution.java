package com.leanxcale.example;

import com.leanxcale.example.avl.AVLTree;
import com.leanxcale.example.avl.AVLTreeDuplicateKeyException;
import com.leanxcale.example.avl.LoanAvlKey;
import com.leanxcale.example.data.LoansReaderService;
import java.io.IOException;
import java.net.URISyntaxException;
import java.sql.SQLException;
import java.util.List;
import javax.annotation.PostConstruct;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

/**
 * Common class for all tests. Just contains the sql code for creating&cleaning the lx database
 */
@Slf4j
public abstract class AbstractTestExecution {

  @Autowired
  LoansReaderService loansReader;

  @Value("${lx.regions}")
  private int REGIONS;

  @Value("${lx.bidimensional.size}")
  private int BIDI_SIZE;

  @Value("${lx.bidimensional.autoremove}")
  private int BIDI_AUTOREMOVE;

  @Value("${insert.mode}")
  private String insertMode;
  protected String insertCommand;
  protected boolean isInsert;

  @Value("${test.duration}")
  protected long duration;

  protected static final String TABLE_NAME = "loans";
  private static final String OA_NAME = "loans_oa";
  protected static final String SQL_CREATE_BASE =
      "create table " + TABLE_NAME + " ("
      + " client_id bigint,"
      + " loan_id bigint,"
      + " ts bigint,"
      + " issue_d bigint,"
      + " loan_status int," // 0 rejected, 1 accepted-paid, 2 accepted-defaulted, 3 accepted-other?
      + " loan_amnt double,"
      + " term int,"
      + " int_rate double,"
      + " installment double,"
      + " grade varchar,"
      + " emp_length double,"
      + " home_ownership varchar,"
      + " annual_inc double,"
      + " verification_status varchar,"
      + " mths_since_issue_d int,"
      + " \"desc\" varchar,"
      + " purpose varchar,"
      + " title varchar,"
      + " zip_code varchar,"
      + " addr_state varchar,"
      + " dti double,"
      + " mths_since_earliest_cr_line int,"
      + " inq_last_6mths double,"
      + " revol_util double,"
      + " out_prncp double,"
      + " total_pymnt double,"
      + " total_rec_int double,"
      + " last_pymnt_d varchar,"
      + " mths_since_last_pymnt_d int,"
      + " mths_since_last_credit_pull_d int,"
      + " total_rev_hi_lim double,"
      + " PRIMARY KEY (client_id, loan_id),"
      + " HASH(client_id) TO DISTRIBUTE  "
      + ")";

  private static final String SQL_CREATE_OA =
      "CREATE ONLINE AGGREGATE "+ OA_NAME +" AS "
          + "  count(*) cnt"
          + " FROM " + TABLE_NAME;


  @PostConstruct
  public void initInsertMode(){
    //adding rows could be done as insert or upsert according to config key
    if (insertMode.equalsIgnoreCase("insert")){
      insertCommand = "insert";
      isInsert = true;
    }else if (insertMode.equalsIgnoreCase("upsert")){
      insertCommand = "upsert";
      isInsert = false;
    }else{
      throw new RuntimeException("Invalid insert.mode: [{"+insertMode+"}]. Only insert|upsert are accepted");
    }
    log.info("Insert command set to:{}", insertCommand);

    if (duration == -1){
      log.info("Got infinite duration");
      duration = Long.MAX_VALUE;
    }else{
      duration = duration * 1000; //convert to ms
    }
  }
  
  protected abstract void execute(String sql) throws SQLException;

  protected void cleanDatabase() throws SQLException {
    log.info("Cleaning database");
    execute("drop table if exists " + TABLE_NAME);
    execute("drop table if exists " + OA_NAME);
  }


  public void initHashDb() throws SQLException {
    cleanDatabase();

    log.info("Creating tables at LeanXcale");
    String lxSql = SQL_CREATE_BASE;
    execute(lxSql);
    execute(SQL_CREATE_OA);
    log.info("Database properly initialized!");
  }

  public void initHashAutosplitDb() throws SQLException {
    cleanDatabase();

    log.info("Creating tables at LeanXcale");

    String lxSql = SQL_CREATE_BASE
        .replaceAll("ts bigint,", "ts bigint SPLIT EVERY " + BIDI_SIZE +" AUTOREMOVE AFTER " + BIDI_AUTOREMOVE + ",") //bidimensional partitioning
        .replaceAll("PRIMARY KEY \\(client_id, loan_id\\)", "PRIMARY KEY (client_id, loan_id, ts)"); //bidimensional field required as part of PK


    execute(lxSql);
    execute(SQL_CREATE_OA);
    log.info("Database properly initialized!");
  }



  public void initPkRangeAutosplitDb() throws IOException, URISyntaxException, SQLException {
    cleanDatabase();

    log.info("Creating tables at LeanXcale");
    String sql = SQL_CREATE_BASE.replaceAll(", HASH\\(client_id\\) TO DISTRIBUTE", "") //remove hash partitioning
        .replaceAll("ts bigint,", "ts bigint SPLIT EVERY " + BIDI_SIZE +" AUTOREMOVE AFTER " + BIDI_AUTOREMOVE + ",") //bidimensional partitioning
        .replaceAll("PRIMARY KEY \\(client_id, loan_id\\)", "PRIMARY KEY (client_id, loan_id, ts)"); //bidimensional field required as part of PK
    execute(sql);
    addPkRangePartitioning(); //split based on the PK
    execute(SQL_CREATE_OA);
    log.info("Database properly initialized!");
  }

  private void addPkRangePartitioning() throws IOException, URISyntaxException, SQLException {
    log.info("Partitioning by PK range");
    List<LoanAvlKey> splitKeys = getSplitPoints();

    for (LoanAvlKey key: splitKeys){
      String sql =
          "ALTER TABLE " + TABLE_NAME + " ADD PARTITION(CLIENT_ID, LOAN_ID) FOR VALUES (" + key.getClientId() + "," + key.getLoanId() + ") MOVE";
      log.info("Adding partition:{}", sql);
      execute(sql);
    }
  }

  protected List<LoanAvlKey> getSplitPoints() throws IOException, URISyntaxException {
    //Read a sampling of a random 1% of the file in order to get the split points
    log.info("Looking for split points");
    AVLTree<LoanAvlKey> avlTree = new AVLTree<>();

    loansReader.readPercentage(1).forEach(loan -> {
      try{
        avlTree.insert(new LoanAvlKey(loan.getClientId(), loan.getId()));
      }catch (AVLTreeDuplicateKeyException e){
        //as we are reading randomly the file may be the chance that we read the same line twice. ignore it and go for the next.
      }
    });
    //    avlTree.printSort();
    List<LoanAvlKey> splitKeys = avlTree.getSplitKeys(REGIONS);
    log.info("Split points established at:{}", splitKeys.toString());
    return splitKeys;
  }

}
