package com.leanxcale.example.jdbcLowLevel;

import com.leanxcale.example.AbstractTestExecution;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;

@Slf4j
public abstract class AbstractJdbcTestExecution extends AbstractTestExecution {

  @Value("${jdbc.lx.driver}")
  private String lxDriver;
  @Value("${jdbc.lx.url}")
  private String lxUrl;
  @Value("${jdbc.lx.user}")
  private String lxUser;
  @Value("${jdbc.lx.password}")
  private String lxPwd;


  protected Connection connect() throws SQLException {
    log.info("Connecting to lx:{}", lxUrl);
    Connection conn = DriverManager.getConnection(lxUrl, lxUser, lxPwd);
    conn.setAutoCommit(false);
    return conn;
  }


  @Override
  protected void execute(String sql) throws SQLException {
    try (Connection connection = connect()){
      connection.createStatement().executeUpdate(sql);
      connection.commit();
    }
  }
}
