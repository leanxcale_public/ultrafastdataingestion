package com.leanxcale.example.jdbcLowLevel;

import com.leanxcale.example.data.Loan;
import com.leanxcale.example.data.LoansReaderService;
import com.leanxcale.example.spring.config.SpringConfig;
import com.leanxcale.example.stats.StatsPrinter;
import java.io.IOException;
import java.net.URISyntaxException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.IntStream;
import lombok.extern.slf4j.Slf4j;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * This test is exactly the same as Test7_Autosplit but should be run agains a scaled out LX
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes= SpringConfig.class)
@Slf4j
public class Test8_HorizontalScalability extends AbstractJdbcTestExecution {

  @Autowired
  LoansReaderService loansReader;

  @Autowired
  StatsPrinter stats;

  @Value("${multithreading.concurrency}")
  private int N_THREADS;

  @Value("${batch.size}")
  private int BATCH_SIZE;

  private Thread mainThread;

  private List<Connection> connections = new ArrayList<>();

  @Before
  public void init() throws SQLException {
    super.initHashAutosplitDb();
  }

  public void  close(){
    log.info("closing [{}] connections", connections.size());
    for (Connection conn: connections){
      try {
        conn.close();
      } catch (SQLException e) {
        log.error("Error closing connection", e);
      }
    }
  }

  @Test
  public void testLx() throws IOException, URISyntaxException {
    stats.start();
    IntStream.range(0, N_THREADS).forEach(x -> {
      new Thread(() -> {
        log.info("Running test thread:{}", Thread.currentThread().getName());
        try {
          doTest(x);
        } catch (Exception e) {
          throw new RuntimeException(e);
        }
      }, "TestThread-" + x).start();
    });

    log.info("Waiting for test ending. Duration: [{}] s", duration);
    this.mainThread = Thread.currentThread();
    try{
      Thread.sleep(duration);
    }catch (InterruptedException e){
      log.trace("Thread interrupted", e);
    }

    close();
    log.info("Ending");
  }

  private void doTest(int threadId) throws IOException, URISyntaxException {
    stats.start();
    long start = System.currentTimeMillis();

    String sql =
        insertCommand + " into loans(loan_id, client_id , ts , issue_d,loan_status, loan_amnt , term , int_rate " +
            ", installment , grade , emp_length , home_ownership" +
            ", annual_inc , verification_status , mths_since_issue_d" +
            ", \"desc\" , purpose , title , zip_code , addr_state" +
            ", dti , mths_since_earliest_cr_line , inq_last_6mths " +
            ", revol_util , out_prncp , total_pymnt , total_rec_int " +
            ", last_pymnt_d , mths_since_last_pymnt_d , mths_since_last_credit_pull_d, total_rev_hi_lim" +
            ") values (?, ? ,? ,? ,?, ?, ? ,? ,? ,?, ?, ? ,? ,? ,?, ?, ? ,? ,? ,?, ?, ? ,? ,? ,?, ?, ? ,? ,? ,?, ?) ";

    try(Connection connection = connect()) { //just open one connection and use it for all the inserts below
      this.connections.add(connection);

      PreparedStatement preparedStatement = connection.prepareStatement(sql);

      while (!isEnd(start)) { //hack to run n-times over the same csv file
        final AtomicInteger i = new AtomicInteger(0);
        this.loansReader.readData(threadId, N_THREADS).forEach(loan -> {
          try {
            Object[] args = getParams(loan);

            for (int j = 0; j < args.length; j++) {
              preparedStatement.setObject(j + 1, args[j]);
            }
            preparedStatement.addBatch();

            if (i.incrementAndGet() % BATCH_SIZE == 0) {
              preparedStatement.executeBatch();
              connection.commit();
              i.set(0);
              stats.incrementInsert(BATCH_SIZE);
            }

          } catch (NullPointerException e){
            //when duration expired and main thread closed the connections , if this thread is justadding batches will fail as a NPE.
            //Do nothing
          } catch (SQLException e) {
            if (e.getMessage().contains("Connection closed")){
              //when duration expired and main thread closed the connections , if this thread is just doing a commit will fail. just ignore it
             //Do nothing
            }
            else {
              throw new RuntimeException(e);
            }
          }
        });
      }
    }catch (SQLException e) {
      throw new RuntimeException(e);
    }
    log.info("Ending");

  }

  private Object[] getParams(Loan loan) {
    Object[] args =
        new Object[]{loan.getId(), loan.getClientId(), loan.getTs(), loan.getIssue_d(), loan.getLoan_status(),
            loan.getLoan_amnt(), loan.getTerm(), loan.getInt_rate(), loan.getInstallment(), loan.getGrade(), loan.getEmp_length(), loan.getHome_ownership(), loan.getAnnual_inc(), loan.getVerification_status(),
            loan.getMths_since_issue_d(), loan.getDesc(), loan.getPurpose(), loan.getTitle(), loan.getZip_code(),
            loan.getAddr_state(), loan.getDti(), loan.getMths_since_earliest_cr_line(), loan.getInq_last_6mths(),
            loan.getRevol_util(), loan.getOut_prncp(), loan.getTotal_pymnt(), loan.getTotal_rec_int(), loan.getLast_pymnt_d(),
            loan.getMths_since_last_pymnt_d(), loan.getMths_since_last_credit_pull_d(), loan.getTotal_rev_hi_lim()};
    return args;
  }

  private boolean isEnd(long start) {
    return (System.currentTimeMillis() - start) >= (duration);
  }

}
