package com.leanxcale.example.jdbcLowLevel;

import com.leanxcale.example.data.Loan;
import com.leanxcale.example.data.LoansReaderService;
import com.leanxcale.example.spring.AbstractSpringTestExecution;
import com.leanxcale.example.spring.config.SpringConfig;
import com.leanxcale.example.stats.StatsPrinter;
import com.leanxcale.exception.LeanxcaleException;
import com.leanxcale.kivi.database.Table;
import com.leanxcale.kivi.session.Session;
import com.leanxcale.kivi.session.SessionFactory;
import com.leanxcale.kivi.session.Settings;
import com.leanxcale.kivi.tuple.Tuple;
import java.io.IOException;
import java.net.URISyntaxException;
import java.sql.SQLException;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.IntStream;
import javax.annotation.Resource;
import lombok.extern.slf4j.Slf4j;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * This test uses the direct kivi-api instead of jdbc to add data.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes= SpringConfig.class)
@Slf4j
public class Test10_NativeKiviApi extends AbstractSpringTestExecution {

  @Resource(name= "lxPermanentJdbcTemplate")
  JdbcTemplate lxJdbcTemplate;  //just for creating tables with parent class

  @Autowired
  LoansReaderService loansReader;

    @Autowired
  StatsPrinter stats;


  @Value("${multithreading.concurrency}")
  private int N_THREADS;

  @Value("${batch.size}")
  private int BATCH_SIZE;
  private Thread mainThread;

  @Value("${kivi.kx.url}")
  private String kiviUrl;


  @Before
  public void init() throws IOException, URISyntaxException, SQLException {
    super.initPkRangeAutosplitDb();
  }

  @Test
  public void testLx() throws InterruptedException, LeanxcaleException {
    test();
  }

  private void test() throws LeanxcaleException {
    Settings settings = Settings.parse(kiviUrl);
    stats.start();

    IntStream.range(0, N_THREADS).forEach(x -> {
      new Thread(() -> {
        log.info("Running test thread:{}", Thread.currentThread().getName());
        try {
          doTest(settings,x);

        } catch (Exception e) {
          throw new RuntimeException(e);
        }
      }, "TestThread-" + x).start();
    });

    log.info("Waiting for test ending. Duration: [{}] s", duration);
    this.mainThread = Thread.currentThread();
    try{
      Thread.sleep(duration);
    }catch (InterruptedException e){
      log.trace("Thread interrupted", e);
    }
    log.info("Ending");
  }

  private void doTest(Settings settings, int threadId) throws IOException, LeanxcaleException, URISyntaxException {
    log.info("Connecting kivi-api with settings:{}", settings);
    try(Session session = SessionFactory.newSession(settings)){
      Table table = session.database().getTable(TABLE_NAME);
      Tuple tuple = table.createTuple();
      final AtomicInteger i = new AtomicInteger(0);

      while (true) {//hack to run n-times over the same csv file
        this.loansReader.readData(threadId, N_THREADS).forEach(loan -> {

          fillTuple(tuple, loan);

          if (isInsert){
            table.insert(tuple);
          }else{
            table.upsert(tuple);
          }

          if (i.incrementAndGet() % BATCH_SIZE == 0) {
            session.commit();
            i.set(0);
            boolean limitReached = stats.incrementInsert(BATCH_SIZE);
            if (limitReached && duration == Long.MAX_VALUE){ //duration set to -1. exit when limit is reached
              log.info("Limit of rows for test reached. Ending test");
              this.mainThread.interrupt();
            }
          }
        });

      }
    }

  }

  private void fillTuple(Tuple tuple, Loan loan) {
    tuple.put("loan_id", loan.getId());
    tuple.put("client_id", loan.getClientId());
    tuple.put("ts", loan.getTs());
    tuple.put("issue_d", loan.getIssue_d());
    tuple.put("loan_status", loan.getLoan_status());
    tuple.put("loan_amnt", loan.getLoan_amnt());
    tuple.put("term", loan.getTerm());
    tuple.put("int_rate", loan.getInt_rate());
    tuple.put("installment", loan.getInstallment());
    tuple.put("grade", loan.getGrade());
    tuple.put("emp_length", loan.getEmp_length());
    tuple.put("home_ownership", loan.getHome_ownership());
    tuple.put("annual_inc", loan.getAnnual_inc());
    tuple.put("verification_status", loan.getVerification_status());
    tuple.put("mths_since_issue_d", loan.getMths_since_issue_d());
    tuple.put("desc", loan.getDesc());
    tuple.put("purpose", loan.getPurpose());
    tuple.put("title", loan.getTitle());
    tuple.put("zip_code", loan.getZip_code());
    tuple.put("addr_state", loan.getAddr_state());
    tuple.put("dti", loan.getDti());
    tuple.put("mths_since_earliest_cr_line", loan.getMths_since_earliest_cr_line());
    tuple.put("inq_last_6mths", loan.getInq_last_6mths());
    tuple.put("revol_util", loan.getRevol_util());
    tuple.put("out_prncp", loan.getOut_prncp());
    tuple.put("total_pymnt", loan.getTotal_pymnt());
    tuple.put("total_rec_int", loan.getTotal_rec_int());
    tuple.put("last_pymnt_d", loan.getLast_pymnt_d());
    tuple.put("mths_since_last_pymnt_d", loan.getMths_since_last_pymnt_d());
    tuple.put("mths_since_last_credit_pull_d", loan.getMths_since_last_credit_pull_d());
    tuple.put("total_rev_hi_lim", loan.getTotal_rev_hi_lim());
  }


  @Override
  protected JdbcTemplate getLxJdbcTemplate() {
    return lxJdbcTemplate;
  }

}
