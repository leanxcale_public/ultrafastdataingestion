package com.leanxcale.example.jdbcLowLevel;

import com.leanxcale.example.data.Loan;
import com.leanxcale.example.data.LoansReaderService;
import com.leanxcale.example.spring.config.SpringConfig;
import com.leanxcale.example.stats.StatsPrinter;
import java.io.IOException;
import java.net.URISyntaxException;
import java.sql.Connection;
import java.sql.SQLException;
import lombok.extern.slf4j.Slf4j;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * This test inserts full sql sentences opening just one connection that remains open and commits for each row.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes= SpringConfig.class)
@Slf4j
public class Test3_PermanentConnection extends AbstractJdbcTestExecution {

  @Autowired
  LoansReaderService loansReader;

  @Autowired
  StatsPrinter stats;


  @Before
  public void init() throws SQLException {
    super.initHashDb();
  }

  @Test
  public void testLx() throws IOException, URISyntaxException {
    test();
  }

  private void test() throws IOException, URISyntaxException {
    stats.start();
    long start = System.currentTimeMillis();

    try(Connection connection = connect()) { //just open one connection and use it for all the inserts below

      while (!isEnd(start)) { //hack to run n-times over the same csv file
        this.loansReader.readData()
            .takeWhile(loan -> !isEnd(start)) //hack to make the test end
            .forEach(loan -> {

              try {
                String sql = generateSql(loan);
                connection.createStatement().executeUpdate(sql); //insert the row
                connection.commit();

                stats.incrementInsert(1);

              } catch (SQLException e) {
                throw new RuntimeException(e);
              }
            });
      }
    } catch (SQLException e) {
      throw new RuntimeException(e);
    }
    log.info("Ending");

  }

  private String generateSql(Loan loan) throws SQLException {
    return
        insertCommand + " into loans(loan_id, client_id , ts , issue_d " +
            ",loan_status, loan_amnt , term , int_rate " + ", installment , grade , emp_length , home_ownership" +
            ", annual_inc , verification_status , mths_since_issue_d" +
            ", \"desc\" , purpose , title , zip_code , addr_state" +
            ", dti , mths_since_earliest_cr_line , inq_last_6mths " +
            ", revol_util , out_prncp , total_pymnt , total_rec_int " +
            ", last_pymnt_d , mths_since_last_pymnt_d , mths_since_last_credit_pull_d " + ", total_rev_hi_lim" + ") "

            + " values (" + loan.getId() + "," + loan.getClientId() + "," + loan.getTs() + "," + loan.getIssue_d() +
            "," + loan.getLoan_status() + "," + loan.getLoan_amnt() + "," + loan.getTerm() + "," + loan.getInt_rate() +
            "," + loan.getInstallment() + "," + "'" + loan.getGrade() + "'" + "," + loan.getEmp_length() + "," + "'" +
            loan.getHome_ownership() + "'" + "," + loan.getAnnual_inc() + "," + "'" + loan.getVerification_status() +
            "'" + "," + loan.getMths_since_issue_d() + "," + "'" + loan.getDesc() + "'" + "," + "'" +
            loan.getPurpose() + "'" + "," + "'" + loan.getTitle() + "'" + "," + "'" + loan.getZip_code() + "'" + "," +
            "'" + loan.getAddr_state() + "'" + "," + loan.getDti() + "," + loan.getMths_since_earliest_cr_line() + "," +
            loan.getInq_last_6mths() + "," + loan.getRevol_util() + "," + loan.getOut_prncp() + "," +
            loan.getTotal_pymnt() + "," + loan.getTotal_rec_int() + "," +
            (!"".equalsIgnoreCase(loan.getLast_pymnt_d()) ? "'" + loan.getLast_pymnt_d() + "'" : "null") + "," +
            loan.getMths_since_last_pymnt_d() + "," + loan.getMths_since_last_credit_pull_d() + "," +
            loan.getTotal_rev_hi_lim() + ")";
  }

  private boolean isEnd(long start) {
    return (System.currentTimeMillis() - start) >= (duration);
  }

}
