package com.leanxcale.example.data;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Loan implements Cloneable{

  private long id;
  private long clientId;
  private long ts;
  private long issue_d;
  private int loan_status;
  private double loan_amnt;
  private int term;
  private double int_rate;
  private double installment ;
  private String grade;
  private double emp_length;
  private String home_ownership ;
  private double annual_inc;
  private String verification_status ;
  private int mths_since_issue_d ;
  private String desc;
  private String purpose;
  private String title ;
  private String zip_code;
  private String addr_state;
  private double dti;
  private int mths_since_earliest_cr_line;
  private double inq_last_6mths;
  private double revol_util;
  private double out_prncp;
  private double total_pymnt;
  private double total_rec_int;
  private String last_pymnt_d;
  private int mths_since_last_pymnt_d;
  private int mths_since_last_credit_pull_d;
  private double total_rev_hi_lim;

  @Override
  public Object clone() throws CloneNotSupportedException {
    return super.clone();
  }
}
