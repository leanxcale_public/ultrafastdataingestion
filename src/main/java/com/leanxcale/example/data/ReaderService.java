package com.leanxcale.example.data;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.stream.Stream;
import lombok.extern.slf4j.Slf4j;

public interface ReaderService<T> {

  Stream<T> readData() throws IOException, URISyntaxException;

  Stream<T>  readData(int section, int totalSections) throws IOException, URISyntaxException;

  Stream<Loan> readPercentage(int percentage) throws IOException, URISyntaxException;
}
