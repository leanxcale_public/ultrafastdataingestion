package com.leanxcale.example.data;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.net.URISyntaxException;
import java.util.Spliterator;
import java.util.function.Consumer;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class FileRandomReadSpliterator implements Spliterator<String> {
  private volatile long totalRead = 0;
  private static int CORES = Runtime.getRuntime().availableProcessors();

  private RandomAccessFile randomAccessFile;
  private String file;
  private long fromByte;
  private long toByte;
  private long target;
  private long length;
  private boolean master;
  private int percentage;

  private long splitSize = -1;
  private int splits = 0;

  public FileRandomReadSpliterator(String file, int percentage) throws URISyntaxException, IOException {
    log.debug("Creating spliterator for file:{}. id:{}", file, this.hashCode());
    this.master = true;
    this.randomAccessFile = new RandomAccessFile(new File(getClass().getClassLoader().getResource(file).toURI()), "r");
    this.file = file;
    this.length = randomAccessFile.length();
    this.target = length * percentage / 100;
    this.fromByte = 0;
    this.toByte = length;
    this.splitSize = this.length / CORES;
    this.percentage = percentage;
  }

  private FileRandomReadSpliterator(String file, long fromByte, long toByte, int percentage) throws URISyntaxException, FileNotFoundException {
    this.master = false;
    this.randomAccessFile = new RandomAccessFile(new File(getClass().getClassLoader().getResource(file).toURI()), "r");
    this.fromByte = fromByte;
    this.toByte = toByte;
    this.length = toByte - fromByte;
    this.target = length * percentage / 100;
    this.percentage = percentage;
    log.debug("Creating split.  from:{}; to:{}; target:{}; length:{}. id:{}", fromByte, toByte, target, length, this.hashCode());
  }

  @Override
  public boolean tryAdvance(Consumer<? super String> action) {
    try {
      if (totalRead >= target) {
        log.debug("target reached({}) reached.  from:{}; to:{}; length:{}", target, fromByte, toByte, length);
        randomAccessFile.close();
        return false;
      }

      long pos = (long) (Math.random() * length) + fromByte;
//      System.out.println("read. from:"+fromByte+", to:"+toByte + "; pos="+pos+";  id:"+this.hashCode());
      randomAccessFile.seek(pos);
      randomAccessFile.readLine(); //discard this (probably) partial line

      String line = randomAccessFile.readLine();//get the next full line
      if (line != null) { //just in case we went to the last line. in that case, discard it and try again
        action.accept(line);
        totalRead += line.getBytes().length;
        if (log.isTraceEnabled()) log.trace("[{}] got line:{}", this.hashCode(), line);
      }

      return true;
    } catch (Exception e) {
      try {
        randomAccessFile.close();
      } catch (IOException ex) {
        throw new RuntimeException(ex);
      }
      throw new RuntimeException(e);
    }
  }


  @SneakyThrows
  @Override
  public Spliterator<String> trySplit() {
    if (!this.master) return null; //only master can create splits
    if (this.splits >= CORES-1) return null; //same split as cores

//    log.debug("trying split from. id:"+this.hashCode());
    long newFrom = this.fromByte;
    long newTo = newFrom + this.splitSize;
    this.fromByte = newTo;
    this.splits++;

    this.length -= this.splitSize;
    this.target = length * percentage / 100;

    return new FileRandomReadSpliterator(this.file,newFrom, newTo, this.percentage);
  }

  @Override
  public long estimateSize() {
    return target;
  }

  @Override
  public int characteristics() {
    int characteristics = Spliterator.IMMUTABLE | Spliterator.NONNULL | Spliterator.SIZED;
    if (!master){
      characteristics |= Spliterator.SUBSIZED;
    }
    return characteristics;
  }
}
