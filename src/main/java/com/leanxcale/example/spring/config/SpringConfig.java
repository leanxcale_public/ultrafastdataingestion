package com.leanxcale.example.spring.config;

import javax.sql.DataSource;
import org.apache.commons.dbcp2.BasicDataSource;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.jdbc.datasource.SingleConnectionDataSource;
import org.springframework.scheduling.annotation.EnableScheduling;

@Configuration
@EnableScheduling
@ComponentScan("com.leanxcale.example")
@PropertySource("classpath:config.properties")
public class SpringConfig {
  
  @Value("${jdbc.lx.driver}")
  private String lxDriver;
  @Value("${jdbc.lx.url}")
  private String lxUrl;
  @Value("${jdbc.lx.user}")
  private String lxUser;
  @Value("${jdbc.lx.password}")
  private String lxPwd;


  @Value("${multithreading.concurrency}")
  private int maxPoolSize;

  private enum DS_TYPE {
    DRIVER, //open&close one connection for each jdbc sentence
    PERMANENT,  //open just 1 connection and keep it open
    POOL //creates a pool of connections. size specified un properties file
  }

  private DataSource createDs(DS_TYPE type, String driver, String url, String user, String pwd) {
    switch (type){
      case DRIVER:
        DriverManagerDataSource ds = new DriverManagerDataSource(); // creates&closes a connection for each jdbc operation
        ds.setDriverClassName(driver);
        ds.setUrl(url);
        ds.setUsername(user);
        ds.setPassword(pwd);
        return ds;

      case PERMANENT:
        ds =  new SingleConnectionDataSource(); //just creates one connection and never close it
        ds.setDriverClassName(driver);
        ds.setUrl(url);
        ds.setUsername(user);
        ds.setPassword(pwd);
        return ds;

      case POOL:
        BasicDataSource dsPool =new BasicDataSource(); //pool apache standard

        dsPool.setDriverClassName(driver);
        dsPool.setUrl(url);
        dsPool.setUsername(user);
        dsPool.setPassword(pwd);
        dsPool.setInitialSize(maxPoolSize);
        dsPool.setMaxTotal(maxPoolSize);
//        dsPool.setValidationQuery("select 1");
        return dsPool;

      default:
        throw new RuntimeException("Unexpect datasource type:"+type);
    }
  }


  private JdbcTemplate getTemplate(DS_TYPE type, String lxDriver, String lxUrl, String lxUser, String lxPwd) {
    DataSource dataSource = createDs(type, lxDriver, lxUrl, lxUser, lxPwd);

    return new JdbcTemplate(dataSource);
  }

  @Bean
  public JdbcTemplate lxJdbcTemplate(){
    return getTemplate(DS_TYPE.DRIVER, lxDriver, lxUrl, lxUser, lxPwd);
  }

  @Bean
  public JdbcTemplate lxPermanentJdbcTemplate(){
    return getTemplate(DS_TYPE.PERMANENT, lxDriver, lxUrl, lxUser, lxPwd);
  }

  @Bean
  public JdbcTemplate lxPoolJdbcTemplate(){
    return getTemplate(DS_TYPE.POOL, lxDriver, lxUrl, lxUser, lxPwd);
  }

}