package com.leanxcale.example.avl;

public class AVLTreeDuplicateKeyException extends RuntimeException{

  public AVLTreeDuplicateKeyException(String msg){
    super(msg);
  }
}
