package com.leanxcale.example.stats;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.atomic.AtomicLong;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class StatsPrinter {

  private static final DateFormat df = new SimpleDateFormat("yyyy-MM-dd_HH.mm.ss");

  private long lastBatchTs = System.currentTimeMillis();
  private AtomicLong batchCounter = new AtomicLong(0);

  private AtomicLong added = new AtomicLong(0);


  private PrintWriter printWriter;

  @Value("${stats.csv}")
  private boolean printCsvStats;

  @Value("${test.batch.stats.size}")
  private long BATCH_STATS_SIZE; // print the time consumed to insert this number of rows

  private boolean started = false;

  @PreDestroy
  public void shutdown(){
    if (printWriter != null){
      printWriter.close();
    }
  }

  @PostConstruct
  public void init() throws IOException {
    if (printCsvStats) {
      printWriter = new PrintWriter(new FileWriter("UFDI_stats_" + df.format(new Date()) + ".csv"));
      printWriter.println("ts,inserts,inserts/sec");
    }
  }

  public void start(){
    this.started = true;
    this.lastBatchTs = System.currentTimeMillis();
    log.info("Running stats printer from ts:{}", this.lastBatchTs);
  }

  /**
   *
   * @param n. elements inserted
   * @return true if the BATCH_STATS_SIZE has been reached. false otherwirse. Usefull if we want to stop the test when the according rows (property test.batch.stats.size) have been inserted
   */
  public boolean incrementInsert(int n){
    this.added.addAndGet(n);

    long batchAdded = this.batchCounter.addAndGet(n);
    if (batchAdded >= BATCH_STATS_SIZE){
      long now = System.currentTimeMillis();
      this.batchCounter.set(0);
      long seconds = (now - lastBatchTs) / 1000;
      log.info("Spent [{}] seconds adding [{}] rows. Insert rate:[{}/sec].", seconds, batchAdded, batchAdded/seconds);
      lastBatchTs = now;

      return true;
    }

    return false;
  }

  @Scheduled(fixedRate = 5000)
  public void print(){
    if (this.started) {
      long c = this.added.getAndSet(0);
      log.info("Added [{}] in the last [{}] seconds. Insert rate:[{}/sec].", c, 5, (c / 5d));
      if (printCsvStats) {
        printWriter.println(System.currentTimeMillis() + "," + c + "," + (c / 5));
      }
    }
  }

}
